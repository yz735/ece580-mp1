# This is for ECE580: Intro to machine learning Spring 2020 in Duke
# This is translated to Python from show_chanWeights.m file provided by Prof. Li by 580 TAs

# import ext libs
import math
import pandas as pd
import numpy as np
import random
from sklearn.model_selection import KFold
from sklearn.linear_model import Lasso
from sklearn.metrics import mean_squared_error
from sklearn.metrics import r2_score
from sklearn.model_selection import train_test_split
from sklearn.utils._testing import ignore_warnings
from sklearn.exceptions import ConvergenceWarning
import matplotlib.pyplot as plt
import seaborn as sns
from collections import defaultdict
import scipy.signal as signal
# from scipy.misc import imread   # Make Sure you install the required packages like Pillow and scipy

def imgRead(fileName):
    """
    load the input image into a matrix
    :param fileName: name of the input file
    :return: a matrix of the input image
    Examples: imgIn = imgRead('lena.bmp')
    """
    imgIn = plt.imread(fileName)
    return imgIn


def imgShow(imgOut):
    """
    show the image saved in a matrix
    :param imgOut: a matrix containing the image to show
    :return: None
    """
    imgOut = np.uint8(imgOut)
    plt.imshow(imgOut,cmap='gray')
    

def getSpatialFreqConst(K, f):
    """
    get the constant alpha_u or beta_u from input parameters
    :param K: dimension of K * K blocks
    :param f: spatial frequency
    :return floating number
    """
    if f==1:
        return (1/K)**0.5
    else:
        return (2/K)**0.5


def basisFunc(K, x, y, u, v):
    """
    get the indices for transformation matrix T
    :param K (int): input block number of pixels on one side
    :param x (int): pixel row index
    :param y (int): pixel col index
    :param u (int): spatial frequency u
    :param v (int): spatial frequency v
    :return floating number
    """
    alpha_u = getSpatialFreqConst(K,u)
    beta_v = getSpatialFreqConst(K,v)
    
    return alpha_u*beta_v*math.cos((math.pi*(2*x-1)*(u-1))/(2*K))*math.cos((math.pi*(2*y-1)*(v-1))/(2*K))
    
    
def calcTransMatrix(K):
    """
    get the transformation matrix T from input matrix image imgIn
    :param K (int): input block number of pixels on one side
    :return transformation matrix in ndarray
    """
    T = np.zeros((K*K, K*K))
    
    for x in range(K):
        for y in range(K):
            for u in range(K):
                for v in range(K):
                    T[x*K+y,u*K+v] = basisFunc(K, x+1, y+1, u+1, v+1)
                    
    return T

@ignore_warnings(category=ConvergenceWarning)
def nRep_CV_Regression_analysis (X, y, reg, M = 3, K = 6):
    """
    perform cross validation analysis on each regression model and printout the information
    @param X (np.ndarray): input feature data
    @param y (np.ndarray): input target variable data
    @param reg: fitting model, e.g. LinearRegression(), Ridge(alpha = 0.1), Lasso(alpha = 0.01)
    @param M (int): number of Repetitions
    @param K (int), default=6: number of cross validation folds
    @return E_MSE (float): average of all Mean Squared Error from each Cross Validation regression
    """

    kf = KFold(n_splits=K, shuffle=True)
    kf.get_n_splits(X)
    MSE = [];
    y_pred = [];

    for i in range(M):
        for train_index, test_index in kf.split(X):
            X_train, X_test = X[train_index], X[test_index]
            y_train, y_test = y[train_index], y[test_index]
            reg.fit(X_train, y_train)
            #print("The fitted R_squared is: ", reg.score(X_test, y_test))
            #print("w_0 is:", reg.intercept_, "w_1 is:", reg.coef_[0], "w_2 is:", reg.coef_[1])
            y_hat = reg.predict(X_test)
            #print("The MSE is: ", mean_squared_error(y_test, y_hat))
            MSE.append(mean_squared_error(y_test, y_hat))
            y_pred.append(np.average(y_hat))
    
    E_MSE = np.average(np.asarray(MSE))
    return E_MSE

@ignore_warnings(category=ConvergenceWarning)
def findBestLambda(X, y, lambda_lower_limit = -6, lambda_higher_limit = 6, n_lambda = 20):
    """
    find the best lambda for this model to have lowest mean squared error
    """
    lambda_list = np.logspace(lambda_lower_limit, lambda_higher_limit, n_lambda, endpoint = True)
    coef_dict = defaultdict(list)

    for lam in lambda_list:
        E_MSE = nRep_CV_Regression_analysis(X, y, Lasso(alpha=lam, fit_intercept = False))
        coef_dict['lambdas'].append(lam)
        coef_dict['E_MSE'].append(E_MSE)
    
    df = pd.DataFrame(coef_dict)
    df = df.sort_values(by = "E_MSE")
    return df.iloc[0][0]


@ignore_warnings(category=ConvergenceWarning)
def recoverOneBlock(blockIn):
    """
    use the lambda with least MSE to find DCT coefficients.
    calculate the recovered block matrix values by multiplying 
    Transformation matrix with found DCT coefficients.
    @param: A (np.ndarray): T[list_spl_idx[i]]. list_spl_idx from samplingBlocksInList
    @param: B (np.ndarray): list_samples[i]. list_samples from samplingBlocksInList
    @param: T (np.ndarray): Transformation matrix from calcTransMatrix
    @param: K (int): number of pixels on one side of the block
    @return: recovered_blk (np.ndarray): recovered block in K*K matrix
    """
    K = blockIn.shape[0]
    T = calcTransMatrix(K)
    
    list_sample_idx = []
    blockIn = blockIn.flatten()
    for i in range(blockIn.shape[0]):
        if blockIn[i] != -1:
            list_sample_idx.append(i)
    
    A = T[list_sample_idx]
    B = blockIn[blockIn!=-1]
    
    best_lambda = findBestLambda(A, B)
    reg = Lasso(alpha=best_lambda, fit_intercept = False)
    reg.fit(A,B)
    recovered_blk = reg.predict(T)
    rounded_recovered_blk = np.zeros((recovered_blk.shape))

    for i in range(recovered_blk.shape[0]):
        rounded_recovered_blk[i] = round(recovered_blk[i])

    recovered_blk = rounded_recovered_blk.reshape((K, K))
    
    return recovered_blk


def divideBlock(imgIn, blkSize):
    """
    Divide the input image into k*k blocks
    @param imgIn (np.ndarray): input image grayscale matrix 
    @param blkSize (int): k of k*k block
    @return (np.ndarray): shape: { block index in row ((imgIn n pixels per row) / blkSize), 
                                   block index in col ((imgIn n pixels per column) / blkSize), 
                                   blkSize, 
                                   blkSize}
    """
    list_blks = [];
    
    nblk_row, nblk_col = int(imgIn.shape[0]/blkSize), int(imgIn.shape[1]/blkSize)
    for i in range(nblk_row):
        list_blk_row = []
        for j in range(nblk_col):
            list_blk_row.append(imgIn[blkSize*i:blkSize*(i+1),blkSize*j:blkSize*(j+1)])
            
        list_blks.append(list_blk_row)
    
    return np.asarray(list_blks);


def getSampledImg(imgIn, blkSize, numSample):
    """
    rebuild the image from sampled pixels and their index
    @param imgIn (np.ndarray): input image grayscale matrix 
    @param blkSize (int): k of k*k block
    @param numSample: total number (int) of pixels taken from the block as samples
    @output: the sampled image rebuilt
    """
    list_blks = divideBlock(imgIn, blkSize)
    
    list_samples, list_spl_idx = samplingBlocksInList(list_blks, numSample)
    
    list_crp_blks = -1 * np.ones((list_samples.shape[0], blkSize**2))
    for i in range(list_samples.shape[0]):
        sample = list_samples[i]
        indexes = list_spl_idx[i]
        for j in range(list_samples.shape[1]):
            list_crp_blks[i][indexes[j]] = sample[j]
    
    nblk_row, nblk_col = int(imgIn.shape[0]/blkSize), int(imgIn.shape[1]/blkSize)
    list_crp_blks_reshaped = list_crp_blks.reshape(nblk_row, nblk_col, blkSize, blkSize)
    return putBlocksTogether(list_crp_blks_reshaped)
    

def putBlocksTogether(list_blks):
    """
    get the image matrix that has the same format with the input image
    @param list_blks (np.ndarray): hold the data for all blocks
                                   shape: { block index in row ((imgIn n pixels per row) / blkSize), 
                                   block index in col ((imgIn n pixels per column) / blkSize), 
                                   blkSize, 
                                   blkSize}
    @return image matrix (np.ndarray)
    """
    row, col, blk_row, blk_col = list_blks.shape
    imgBlocksPutTogether = -1 * np.ones((row*blk_row, col*blk_col))

    for row_i in range(row):
        for col_i in range(col):
            for blk_row_i in range(blk_row):
                for blk_col_i in range(blk_col):
                    imgBlocksPutTogether[row_i*blk_row+blk_row_i]\
                    [col_i*blk_col+blk_col_i] = list_blks[row_i][col_i][blk_row_i][blk_col_i]

    return imgBlocksPutTogether


def samplingBlocksInList(list_blks, numSample):
    """
    sampling (sense) numSample of pixels in each block
    from list_blks returned by method divideBlock
    record the sensed pixel value and their coordinate index
    used random.sample method to avoid repetition in sampling
    @param list_blks (np.ndarray): shape: { block index in row ((imgIn n pixels per row) / blkSize), 
                                           block index in col ((imgIn n pixels per column) / blkSize), 
                                           blkSize, 
                                           blkSize}
    @param numSample: total number (int) of pixels taken from the block as samples
    @return (np.ndarray): shape: { block index in row ((imgIn n pixels per row) / blkSize), 
                                   block index in col ((imgIn n pixels per column) / blkSize), 
                                   blkSize, 
                                   blkSize}               
    """
    ans_blks = -1 * np.ones((list_blks.shape))
    for i in range(list_blks.shape[0]):
        for j in range(list_blks.shape[1]):
            block = list_blks[i][j]
            sampled_blk = block.flatten()
            n_pix = sampled_blk.shape[0]
            random_idx = random.sample(range(sampled_blk.shape[0]), numSample)
            ans_block = ans_blks[i][j].flatten()
            for idx in random_idx:
                ans_block[idx] = sampled_blk[idx]
            ans_blks[i][j] = ans_block.reshape((block.shape))
            
    return ans_blks

def imgRecover(imgIn, blkSize, numSample, samplingFirst = True):
    """
    Recover the input image from a small size samples
    :param imgIn: input image
    :param blkSize: block size
    :param numSample: how many samples in each block
    :param medFilter = False: whether or not to apply median filter to the recovered image
    :return: recovered image
    """
    ##### Your Implementation here
    list_blks = divideBlock(imgIn, blkSize)
    list_samples = samplingBlocksInList(list_blks, numSample)
    
    n_blk_row, n_blk_col = list_samples.shape[0], list_samples.shape[1]
    
    list_recovered_blks = -1 * np.ones((list_samples.shape))
    for i in range(n_blk_row):
        for j in range(n_blk_col):
            list_recovered_blks[i][j] = recoverOneBlock(list_samples[i][j])
    
    recoveredImg = putBlocksTogether(list_recovered_blks)
      
    return recoveredImg

"""
if __name__ == '__main__':
    a = imgRead('lena.bmp')
    print(np.shape(a))
    imgShow(a)
    print(a)
"""